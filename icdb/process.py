"""
        ____
   ____/ / /_
  / __  / __/
 / /_/ / /_
 \__,_/\__/     dealertrack technologies

Welcome to the Internet Car Database v{}
"""
import csv
import argparse
import logging

from . import __version__
from .db import db_cursor

# Setup logging
logger = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(name)s %(levelname)s %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.ERROR)


def _get_car_id(car_id=None):
    """ Keep asking the user for a car id until it is finally valid.
    """
    if car_id:
        if car_id.isdigit():
            return car_id

    car_id = get_input("{:>20}".format("Enter car id [1]: "))
    return _get_car_id(car_id)


def _print_table(*rows):
    LAYOUT = "{:<4} {:<4} {:20} {:20}"
    print LAYOUT.format("Id", "Year", "Make", "Model")
    print LAYOUT.format("-" * 4, "-" * 4, "-" * 20, "-" * 20)
    for row in rows:
        print LAYOUT.format(*row)


def get_input(question):
    return raw_input(question)


def _get_year(year=None):
    """ Keep asking the user for a year until it is finally valid.
    """
    if year:
        if year.isdigit():
            if len(year) == 4:
                return year

    year = get_input("{:>25}".format("Enter car year [2013]: "))
    return _get_year(year)


def create_car():
    """ Let the user input parameters to create a new car, and
    insert it to the database.
    """
    with db_cursor() as db:
        year = _get_year()
        make = get_input("{:>24}".format("Enter car make [Toyota]: "))
        model = get_input("{:>25}".format("Enter car model [Camry]: "))

        row = (year, make, model)

        if not all(row):
            logger.error("One or more of your answers were empty, please try again.")
            return

        print "Inserting a {} {} {} to the database...".format(*row)
        db.execute("INSERT INTO cars (year, make, model) VALUES (?, ?, ?)", row)


def import_cars(csv_file):
    """ Replace the data in the database with data from a CSV file.
    """
    rows = []
    with open(csv_file) as f:
        reader = csv.DictReader(f)
        rows = [(row["year"], row["make"], row["model"]) for row in reader]

    with db_cursor() as db:
        print "Replacing {} values in the databases with data from '{}'.".format(len(rows), csv_file)

        # truncate table data and reset autoincrement
        db.execute("DELETE FROM cars;")
        db.execute("DELETE FROM SQLITE_SEQUENCE WHERE name='cars';")
        db.executemany("INSERT INTO cars (year, make, model) VALUES (?, ?, ?)", rows)


def update_car():
    """ Let the user input parameters to update a car in the database.
    """
    with db_cursor() as db:
        car_id = _get_car_id()

        row = db.execute("SELECT * FROM cars WHERE id = (?)", (car_id,)).fetchone()
        if not row:
            print "Car not found in the database."
            return

        _print_table(row)

        year = _get_year()
        make = get_input("{:>24}".format("Enter car make [Toyota]: "))
        model = get_input("{:>25}".format("Enter car model [Camry]: "))

        row = (year, make, model)

        if not all(row):
            logger.error("One or more of your answers were empty, please try again.")
            return

        print "Updating row in the database..."
        db.execute(
            "UPDATE cars SET year=?, make=?, model=? WHERE id=?",
            (year, make, model, car_id))


def delete_car():
    """ Let the user input parameters to delete a car from the database.
    """
    with db_cursor() as db:
        car_id = _get_car_id()

        row = db.execute(
            "SELECT * FROM cars WHERE id=(?)",
            (car_id,)).fetchone()

        if not row:
            print "Car not found in the database."
            return

        _print_table(row)

        if get_input("Delete this row? [yes/no]: ") == "yes":
            print "Deleting car from the database..."
            db.execute("DELETE FROM cars WHERE id=(?)", (car_id,))


def list_cars():
    """ List all cars in the database.
    """
    with db_cursor() as db:
        rows = db.execute("SELECT * FROM cars").fetchall()
        if rows:
            _print_table(*rows)


def main():
    parser = argparse.ArgumentParser(description=__doc__.format(__version__),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='be verbose')

    actions = parser.add_mutually_exclusive_group(required=True)
    actions.add_argument('-i', '--importcsv',
                         action='store',
                         help='import a CSV file, overwriting the database')
    actions.add_argument('-a', '--add',
                         action='store_true',
                         help='add a car to the database')
    actions.add_argument('-u', '--update',
                         action='store_true',
                         help='update a car in the database')
    actions.add_argument('-d', '--delete',
                         action='store_true',
                         help='delete a car from the database')
    actions.add_argument('-l', '--listcars',
                         action='store_true',
                         help='list the cars in the database')

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.INFO)

    if args.importcsv:
        logger.info("importing csv")
        import_cars(args.importcsv)

    if args.add:
        logger.info("adding new car")
        create_car()

    if args.update:
        logger.info("updating a car")
        update_car()

    if args.delete:
        logger.info("deleting a car")
        delete_car()

    if args.listcars:
        logger.info("listing cars")
        list_cars()
